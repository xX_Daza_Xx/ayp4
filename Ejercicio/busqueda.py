import tkinter as tk
from tkinter import messagebox
import hashlib
import random
import time

tabla_hash = None
cantidad = None
inicio_generacion = None
fin_generacion = None
label_resultado = None

class TablaHash:
    def __init__(self, capacidad):
        self.capacidad = capacidad
        self.tabla = [[] for _ in range(capacidad)]

    def funcion_hash(self, clave):
        return clave % self.capacidad

    def agregar(self, clave, valor):
        indice = self.funcion_hash(clave)
        while self.tabla[indice]:
            indice = (indice + 1) % self.capacidad
        self.tabla[indice].append((clave, valor))

    def obtener(self, clave):
        indice = self.funcion_hash(clave)
        indice_inicial = indice
    # Iterar sobre los pares clave-valor en la tabla hash
        while self.tabla[indice]:
            if self.tabla[indice][0][0] == clave:
                return self.tabla[indice][0][1], indice
            indice = (indice + 1) % self.capacidad
            if indice == indice_inicial:
                break
    # Si no se encuentra la clave, devolver None y el índice inicial
        return None, indice_inicial

def buscar():
    global tabla_hash, label_resultado
    clave_buscar = entry_clave.get()
    if not clave_buscar.isdigit():
        messagebox.showerror("Error", "Por favor, ingrese una clave válida (número entero).")
        return
    inicio_busqueda = time.perf_counter_ns()
    valor, indice_actual = tabla_hash.obtener(int(clave_buscar))
    fin_busqueda = time.perf_counter_ns()
    if valor is not None:
        hash_clave = hashlib.sha256(str(clave_buscar).encode()).hexdigest()
        mensaje = f"El valor asociado a la clave {clave_buscar} es: {valor}\nPosición actual en la tabla: {indice_actual}\nHash SHA-256: {hash_clave}"
        indice_anterior = int(clave_buscar) % 500
        if indice_actual != indice_anterior:
            mensaje += f"\nPosición anterior en la tabla: {indice_anterior}"
        label_resultado.config(text=mensaje)
    else:
        label_resultado.config(text=f"No se encontró ningún valor asociado a la clave {clave_buscar}")
    tiempo_busqueda = fin_busqueda - inicio_busqueda
    label_tiempo_busqueda.config(text=f"Tiempo de búsqueda: {tiempo_busqueda} nanosegundos")

def mostrar_tabla():
    global tabla_hash, inicio_generacion, fin_generacion
    if tabla_hash is None:
        return
    tabla_str = ""
    for i in range(tabla_hash.capacidad):
        if tabla_hash.tabla[i]:
            tabla_str += f"Cubo {i}: Clave: {tabla_hash.tabla[i][0][0]}, SHA-256: {tabla_hash.tabla[i][0][1]}, Posición actual en la tabla: {i}\n"
        else:
            tabla_str += f"Cubo {i}: Vacío\n"
    textbox_tabla.delete("1.0", tk.END)
    textbox_tabla.insert(tk.END, tabla_str)
    if inicio_generacion is not None and fin_generacion is not None:
        label_tiempo_generacion_insercion.config(text=f"Tiempo de generación e inserción de datos: {fin_generacion - inicio_generacion} nanosegundos")

def generar_tabla():
    global tabla_hash, cantidad, inicio_generacion, fin_generacion
    cantidad = int(entry_cantidad.get())
    if cantidad >= 500:
        messagebox.showerror("Error", "Por favor, ingrese una cantidad menor a 500.")
        return
    inicio_generacion = time.perf_counter_ns()
    numeros_generados = set()
    tabla_hash = TablaHash(500)

    while len(numeros_generados) < cantidad:
        numero_aleatorio = random.randint(1, 5000)
        if numero_aleatorio not in numeros_generados:
            hash_clave = hashlib.sha256(str(numero_aleatorio).encode()).hexdigest()
            tabla_hash.agregar(numero_aleatorio, hash_clave)
            numeros_generados.add(numero_aleatorio)

    fin_generacion = time.perf_counter_ns()
    mostrar_tabla()

root = tk.Tk()
root.title("Tabla Hash")

frame_cantidad = tk.Frame(root)
frame_cantidad.pack(pady=10)

label_cantidad = tk.Label(frame_cantidad, text="Cantidad de datos:")
label_cantidad.pack(side=tk.LEFT)

entry_cantidad = tk.Entry(frame_cantidad)
entry_cantidad.pack(side=tk.LEFT)

button_generar_tabla = tk.Button(frame_cantidad, text="Generar Tabla", command=generar_tabla)
button_generar_tabla.pack(side=tk.LEFT, padx=10)

frame_buscar = tk.Frame(root)
frame_buscar.pack(pady=10)

label_clave = tk.Label(frame_buscar, text="Clave:")
label_clave.pack(side=tk.LEFT)

entry_clave = tk.Entry(frame_buscar)
entry_clave.pack(side=tk.LEFT)

button_buscar = tk.Button(frame_buscar, text="Buscar", command=buscar)
button_buscar.pack(side=tk.LEFT, padx=10)

frame_tabla = tk.Frame(root)
frame_tabla.pack(pady=10)

textbox_tabla = tk.Text(frame_tabla, width=80, height=30)
textbox_tabla.pack()

button_mostrar_tabla = tk.Button(root, text="Mostrar Tabla Hash", command=mostrar_tabla)
button_mostrar_tabla.pack(pady=10)

label_tiempo_generacion_insercion = tk.Label(root, text="")
label_tiempo_generacion_insercion.pack()

label_tiempo_busqueda = tk.Label(root, text="")
label_tiempo_busqueda.pack()

label_resultado = tk.Label(root, text="")
label_resultado.pack()

root.mainloop()