import tkinter as tk
from tkinter import messagebox
import hashlib
import random
import time
import matplotlib.pyplot as plt

tabla_hash = None
cantidad = None
inicio_generacion = None
fin_generacion = None
label_resultado = None

tiempos_quicksort = []
tiempos_shellsort = []
tiempos_heapsort = []
tiempos_radixsort = []
tiempos_bucketsort = []

class TablaHash:
    def __init__(self, capacidad):
        self.capacidad = capacidad
        self.tabla = [[] for _ in range(capacidad)]

    def funcion_hash(self, clave):
        return clave % self.capacidad

    def agregar(self, clave, valor):
        indice = self.funcion_hash(clave)
        while self.tabla[indice]:
            indice = (indice + 1) % self.capacidad
        self.tabla[indice].append((clave, valor))

    def obtener(self, clave):
        indice = self.funcion_hash(clave)
        indice_inicial = indice
        while self.tabla[indice]:
            if self.tabla[indice][0][0] == clave:
                return self.tabla[indice][0][1], indice
            indice = (indice + 1) % self.capacidad
            if indice == indice_inicial:
                break
        return None, indice_inicial

    


def buscar():
    global tabla_hash

    # Obtener la clave a buscar
    clave_buscar = entry_clave.get()
    if not clave_buscar.isdigit():
        messagebox.showerror("Error", "Por favor, ingrese una clave válida (número entero).")
        return

    # Realizar la búsqueda y medir el tiempo
    inicio_busqueda = time.perf_counter_ns()
    valor, indice_actual = tabla_hash.obtener(int(clave_buscar))
    fin_busqueda = time.perf_counter_ns()

    # Crear una nueva ventana emergente para mostrar la información de la búsqueda y el tiempo
    ventana_resultado = tk.Toplevel(root)
    ventana_resultado.title("Resultado de la Búsqueda")

    # Crear etiquetas para mostrar la información de la búsqueda y el tiempo
    label_resultado_busqueda = tk.Label(ventana_resultado, text="")
    label_resultado_busqueda.pack()

    tiempo_busqueda = fin_busqueda - inicio_busqueda
    label_tiempo_busqueda = tk.Label(ventana_resultado, text=f"Tiempo de búsqueda: {tiempo_busqueda} nanosegundos")
    label_tiempo_busqueda.pack()

    # Actualizar la etiqueta de resultado de la búsqueda con el valor encontrado, si lo hay
    if valor is not None:
        hash_clave = hashlib.sha256(str(clave_buscar).encode()).hexdigest()
        mensaje = f"El valor asociado a la clave {clave_buscar} es: {valor}\nPosición actual en la tabla: {indice_actual}\nHash SHA-256: {hash_clave}"
        indice_anterior = int(clave_buscar) % 500
        if indice_actual != indice_anterior:
            mensaje += f"\nPosición anterior en la tabla: {indice_anterior}"
        label_resultado_busqueda.config(text=mensaje)
    else:
        label_resultado_busqueda.config(text=f"No se encontró ningún valor asociado a la clave {clave_buscar}")
    tiempo_busqueda = fin_busqueda - inicio_busqueda
    label_tiempo_busqueda.config(text=f"Tiempo de búsqueda: {tiempo_busqueda} nanosegundos")

def mostrar_tabla():
    global tabla_hash, inicio_generacion, fin_generacion
    if tabla_hash is None:
        return
    tabla_str = ""
    for i in range(tabla_hash.capacidad):
        if tabla_hash.tabla[i]:
            tabla_str += f"Cubo {i}: Clave: {tabla_hash.tabla[i][0][0]}, SHA-256: {tabla_hash.tabla[i][0][1]}, Posición actual en la tabla: {i}\n"
        else:
            tabla_str += f"Cubo {i}: Vacío\n"
    textbox_tabla.delete("1.0", tk.END)
    textbox_tabla.insert(tk.END, tabla_str)
    if inicio_generacion is not None and fin_generacion is not None:
        label_tiempo_generacion_insercion.config(text=f"Tiempo de generación e inserción de datos: {fin_generacion - inicio_generacion} nanosegundos")

def generar_tabla():
    global tabla_hash, cantidad, inicio_generacion, fin_generacion
    cantidad = int(entry_cantidad.get())
    espacio=10
    if cantidad >= 10:
        espacio=cantidad
        
    inicio_generacion = time.perf_counter_ns()
    numeros_generados = set()
    tabla_hash = TablaHash(espacio)

    while len(numeros_generados) < cantidad:
        numero_aleatorio = random.randint(1, 5000)
        if numero_aleatorio not in numeros_generados:
            hash_clave = hashlib.sha256(str(numero_aleatorio).encode()).hexdigest()
            tabla_hash.agregar(numero_aleatorio, hash_clave)
            numeros_generados.add(numero_aleatorio)

    fin_generacion = time.perf_counter_ns()
    mostrar_tabla()

def quicksort(lista):
    if len(lista) <= 1:
        return lista
    else:
        pivote = lista[len(lista) // 2]
        menores = [x for x in lista if x[0] < pivote[0]]
        iguales = [x for x in lista if x[0] == pivote[0]]
        mayores = [x for x in lista if x[0] > pivote[0]]
        return quicksort(menores) + iguales + quicksort(mayores)

def bucketsort(lista):
    if not lista:
        return lista
    max_value = max(lista)
    size = max_value // len(lista)
    buckets = [[] for _ in range(size + 1)]
    for i in lista:
        j = i // len(lista)
        if j != size:
            buckets[j].append(i)
        else:
            buckets[size - 1].append(i)
    result = []
    for b in buckets:
        result.extend(sorted(b))
    return result

def shellsort(lista):
    gap = len(lista) // 2
    while gap > 0:
        for i in range(gap, len(lista)):
            current_item = lista[i]
            j = i
            while j >= gap and lista[j - gap] > current_item:
                lista[j] = lista[j - gap]
                j -= gap
            lista[j] = current_item
        gap //= 2
    return lista

def radixsort(lista):
    RADIX = 10
    placement = 1
    max_digit = max(lista)

    while placement < max_digit:
        buckets = [list() for _ in range(RADIX)]
        for i in lista:
            tmp = int((i / placement) % RADIX)
            buckets[tmp].append(i)
        a = 0
        for b in range(RADIX):
            buck = buckets[b]
            for i in buck:
                lista[a] = i
                a += 1
        placement *= RADIX
    return lista

def heapsort(lista):
    def heapify(lista, start, end):
        root = start
        while 2 * root + 1 <= end:
            child = 2 * root + 1
            if child + 1 <= end and lista[child] < lista[child + 1]:
                child += 1
            if child <= end and lista[root] < lista[child]:
                lista[root], lista[child] = lista[child], lista[root]
                root = child
            else:
                return

    n = len(lista)
    for i in range(n // 2, -1, -1):
        heapify(lista, i, n - 1)

    for i in range(n - 1, 0, -1):
        lista[i], lista[0] = lista[0], lista[i]
        heapify(lista, 0, i - 1)
    return lista

def mostrar_tabla():
    global tabla_hash, inicio_generacion, fin_generacion
    if tabla_hash is None:
        return
    tabla_str = ""
    for i in range(tabla_hash.capacidad):
        if tabla_hash.tabla[i]:
            elementos = tabla_hash.tabla[i]
            elementos_ordenados = quicksort(elementos)
            for clave, valor in elementos_ordenados:
                tabla_str += f"Cubo {i}: Clave: {clave}, SHA-256: {valor}, Posición actual en la tabla: {i}\n"
        else:
            tabla_str += f"Cubo {i}: Vacío\n"
    textbox_tabla.delete("1.0", tk.END)
    textbox_tabla.insert(tk.END, tabla_str)
    if inicio_generacion is not None and fin_generacion is not None:

        label_tiempo_generacion_insercion.config(text=f"Tiempo de generación e inserción de datos: {fin_generacion - inicio_generacion} nanosegundos")
                
        label_tiempo_quick.config(text=f"Tiempos de ordenamiento\nQuicksort: {tiempo_de_ejecucion(quicksort, elementos)} nanosegundos")

        label_tiempo_shell.config(text=f"Shellsort: {tiempo_de_ejecucion(shellsort, elementos)} nanosegundos ")

        label_tiempo_heap.config(text=f"Heapsort: {tiempo_de_ejecucion(heapsort, elementos)} nanosegundos ")

        label_tiempo_radix.config(text=f"Radixsort: {tiempo_de_ejecucion(radixsort, [e[0] for e in elementos])} nanosegundos ")

        label_tiempo_bucket.config(text=f"Bucketsort: {tiempo_de_ejecucion(bucketsort, [e[0] for e in elementos])} nanosegundos ")

def tiempo_de_ejecucion(funcion, lista):
    start = time.perf_counter_ns()
    funcion(lista.copy())
    end = time.perf_counter_ns()
    return end - start

def medir_tiempos_ordenamiento():
    global tiempos_quicksort, tiempos_shellsort, tiempos_heapsort, tiempos_radixsort, tiempos_bucketsort
    # Limpiar las listas de tiempos de ordenamiento
    tiempos_quicksort.clear()
    tiempos_shellsort.clear()
    tiempos_heapsort.clear()
    tiempos_radixsort.clear()
    tiempos_bucketsort.clear()

    tamanos = [1000, 2000, 3000, 4000, 5000]
    for n in tamanos:
        numeros_generados = [random.randint(1, 7000) for _ in range(n)]
        tiempos_quicksort.append(tiempo_de_ejecucion(quicksort, [(num, "") for num in numeros_generados]))
        tiempos_shellsort.append(tiempo_de_ejecucion(shellsort, [(num, "") for num in numeros_generados]))
        tiempos_heapsort.append(tiempo_de_ejecucion(heapsort, [(num, "") for num in numeros_generados]))
        tiempos_radixsort.append(tiempo_de_ejecucion(radixsort, [num for num in numeros_generados]))
        tiempos_bucketsort.append(tiempo_de_ejecucion(bucketsort, [num for num in numeros_generados]))
    graficar_tiempos(tamanos)


def graficar_tiempos(tamanos):
    plt.plot(tamanos, tiempos_quicksort, label='Quicksort')
    plt.plot(tamanos, tiempos_shellsort, label='Shellsort')
    plt.plot(tamanos, tiempos_heapsort, label='Heapsort')
    plt.plot(tamanos, tiempos_radixsort, label='Radixsort')
    plt.plot(tamanos, tiempos_bucketsort, label='Bucketsort')
    plt.xlabel('Número de datos')
    plt.ylabel('Tiempo de ordenamiento (1000000ns)')
    plt.title('Tiempos de Ordenamiento')
    plt.legend()
    plt.gca().set_ylim([0, 15000000])  # Ajustamos el límite superior del eje y a 20 millones de nanosegundos
    fontsize = 6
    for i, txt in enumerate(tiempos_quicksort):
        plt.annotate(f'{txt}', (tamanos[i], tiempos_quicksort[i]), textcoords="offset points", xytext=(0,10), ha='center', fontsize=fontsize)
    for i, txt in enumerate(tiempos_shellsort):
        plt.annotate(f'{txt}', (tamanos[i], tiempos_shellsort[i]), textcoords="offset points", xytext=(0,10), ha='center', fontsize=fontsize)
    for i, txt in enumerate(tiempos_heapsort):
        plt.annotate(f'{txt}', (tamanos[i], tiempos_heapsort[i]), textcoords="offset points", xytext=(0,10), ha='center', fontsize=fontsize)
    for i, txt in enumerate(tiempos_radixsort):
        plt.annotate(f'{txt}', (tamanos[i], tiempos_radixsort[i]), textcoords="offset points", xytext=(0,10), ha='center', fontsize=fontsize)
    for i, txt in enumerate(tiempos_bucketsort):
        plt.annotate(f'{txt}', (tamanos[i], tiempos_bucketsort[i]), textcoords="offset points", xytext=(0,10), ha='center', fontsize=fontsize)

    plt.show()

    #Ventana de dialogo para mostrar los tiempos de ordenamiento
    messagebox.showinfo("Información", "Se han calculado los tiempos de ordenamiento de forma exitosa")
    messagebox.showinfo("Tiempos de ordenamiento", f"Quicksort: {tiempos_quicksort}\nShellsort: {tiempos_shellsort}\nHeapsort: {tiempos_heapsort}\nRadixsort: {tiempos_radixsort}\nBucketsort: {tiempos_bucketsort}")


root = tk.Tk()
root.title("Tabla Hash")

frame_cantidad = tk.Frame(root)
frame_cantidad.pack(pady=10)

label_cantidad = tk.Label(frame_cantidad, text="Cantidad de datos:")
label_cantidad.pack(side=tk.LEFT)

entry_cantidad = tk.Entry(frame_cantidad)
entry_cantidad.pack(side=tk.LEFT)

button_generar_tabla = tk.Button(frame_cantidad, text="Generar Tabla", command=generar_tabla)
button_generar_tabla.pack(side=tk.LEFT, padx=10)

button_medir_tiempos = tk.Button(frame_cantidad, text="Medir Tiempos de Ordenamiento", command=medir_tiempos_ordenamiento)
button_medir_tiempos.pack(side=tk.LEFT, padx=10)

frame_buscar = tk.Frame(root)
frame_buscar.pack(pady=10)

label_clave = tk.Label(frame_buscar, text="Clave:")
label_clave.pack(side=tk.LEFT)

entry_clave = tk.Entry(frame_buscar)
entry_clave.pack(side=tk.LEFT)

button_buscar = tk.Button(frame_buscar, text="Buscar", command=buscar)
button_buscar.pack(side=tk.LEFT, padx=10)

frame_tabla = tk.Frame(root)
frame_tabla.pack(pady=10)

textbox_tabla = tk.Text(frame_tabla, width=80, height=30)
textbox_tabla.pack()

button_mostrar_tabla = tk.Button(root, text="Mostrar Tabla Hash", command=mostrar_tabla)
button_mostrar_tabla.pack(pady=10)

label_tiempo_generacion_insercion = tk.Label(root, text="")
label_tiempo_generacion_insercion.pack()

label_tiempo_quick = tk.Label(root, text="")
label_tiempo_quick.pack()

label_tiempo_bucket = tk.Label(root, text="")
label_tiempo_bucket.pack()

label_tiempo_shell = tk.Label(root, text="")
label_tiempo_shell.pack()

label_tiempo_radix = tk.Label(root, text="")
label_tiempo_radix.pack()

label_tiempo_heap = tk.Label(root, text="")
label_tiempo_heap.pack()

label_tiempo_busqueda = tk.Label(root, text="")
label_tiempo_busqueda.pack()

label_resultado = tk.Label(root, text="")
label_resultado.pack()

root.mainloop()