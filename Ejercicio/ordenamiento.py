import random
import time
def generar_lista(cantidad, maximo, minimo):
    lista = []
    for _ in range(cantidad):
        dato_aleatorio=random.randint(minimo,maximo)
        lista.append(dato_aleatorio)
    return lista

def quicksort(lista):
  if len(lista) <= 1:
    return lista
  pivote = lista[0]
  izquierda = [elemento for elemento in lista[1:] if elemento < pivote]
  derecha = [elemento for elemento in lista[1:] if elemento >= pivote]
  return quicksort(izquierda) + [pivote] + quicksort(derecha)

def shellsort(lista):
  gap = len(lista) // 2
  while gap > 0:
    for i in range(gap, len(lista)):
        current_item = lista[i]
        j = i
        while j >= gap and lista[j - gap] > current_item:
            lista[j] = lista[j - gap]
            j -= gap
        lista[j] = current_item
    gap //= 2
  return lista

def heapsort(lista):
    def heapify(lista, start, end):
        root = start
        while 2 * root + 1 <= end:
            child = 2 * root + 1
            if child + 1 <= end and lista[child] < lista[child + 1]:
                child += 1
            if child <= end and lista[root] < lista[child]:
                lista[root], lista[child] = lista[child], lista[root]
                root = child
            else:
                return

    n = len(lista)
    for i in range(n // 2, -1, -1):
        heapify(lista, i, n - 1)

    for i in range(n - 1, 0, -1):
        lista[i], lista[0] = lista[0], lista[i]
        heapify(lista, 0, i - 1)
    return lista

def radixsort(lista):
    RADIX = 10
    placement = 1
    max_digit = max(lista)

    while placement < max_digit:
      buckets = [list() for _ in range( RADIX )]
      for i in lista:
        tmp = int((i / placement) % RADIX)
        buckets[tmp].append(i)
      a = 0
      for b in range( RADIX ):
        buck = buckets[b]
        for i in buck:
          lista[a] = i
          a += 1
      placement *= RADIX
    return lista

def bucketsort(lista):
    max_value = max(lista)
    buckets = [list() for _ in range(max_value + 1)]
    for i in lista:
        buckets[i].append(i)
    a = 0
    for b in range(max_value + 1):
        buck = buckets[b]
        for i in buck:
          lista[a] = i
          a += 1
    return lista



#Inicia el main
print("Este algoritmo ordena una lista de datos que se generan de forma aleatoria segun las condiciones que ponga el usuario\n")
cantidad = int(input("Ingrese la cantidad de datos que tiene la lista a ordenar\n"))

maximo = int(input("Ingrese el valor maximo de la lista\n"))
minimo = int(input("Ingrese el valor minimo de la lista\n"))
lista_datos=generar_lista(cantidad, maximo, minimo)

print("La lista generada es: ", lista_datos)
print("La lista ordenada Quick es: ", quicksort(lista_datos))
print("La lista ordenada Shell es: ", shellsort(lista_datos))
print("La lista ordenada Heap es: ", heapsort(lista_datos))
print("La lista ordenada radix es: ", radixsort(lista_datos))
print("La lista ordenada bucket es: ", bucketsort(lista_datos))

