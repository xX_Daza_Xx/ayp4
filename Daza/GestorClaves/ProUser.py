import tkinter as tk
from tkinter import ttk, messagebox
import re

Archivo = "Daza/GestorClaves/usuarios.txt"

def encriptar(mensaje, token):
    cifrado = ""
    for caracter in mensaje:
        if caracter.isalpha():
            ascii_offset = 65 if caracter.isupper() else 97
            cifrado += chr((ord(caracter) - ascii_offset + token) % 26 + ascii_offset)
        else:
            cifrado += caracter
    return cifrado

def desencriptar(mensaje, token):
    descifrado = ""
    for caracter in mensaje:
        if caracter.isalpha():
            ascii_offset = 65 if caracter.isupper() else 97
            descifrado += chr((ord(caracter) - ascii_offset - token) % 26 + ascii_offset)
        else:
            descifrado += caracter
    return descifrado

def token(fecha):
    fecha_sin_barras = fecha.replace("/", "")
    numeros = list(map(int, fecha_sin_barras))
    suma = sum(numeros)
    return suma

def procesar_datos(contenido):
  contenido.append([entry_tdoc.get(), entry_nuip.get(), entry_name.get(), encriptar(entry_pass.get(),token(entry_fechaNaci.get())), entry_fechaNaci.get(), entry_city.get(), entry_mail.get(), entry_university.get()])

def leer_archivo():  
  with open(Archivo, "r") as archivo:
    contenido = []
    for linea in archivo:
      linea = linea.strip()
      elementos_linea = linea.split(",")
      contenido.append(elementos_linea)
  return contenido

def generar_archivo(contenido):
  procesar_datos(contenido)
  try:
    with open(Archivo, "w") as archivo:
      for fila in contenido:
        archivo.write(','.join(map(str, fila)) + '\n')
    archivo.close()
    print(f"El archivo {Archivo} se ha creado exitosamente.")
  except Exception as error:
    messagebox.showerror("ERROR", "Error al crear el archivo: " + str(error))

def validar_fecha():
  try:
    expresion_regular = r"^(0?[1-9]|[1-2][0-9]|3[0-1])\/(0?[1-9]|1[0-2])\/(19[0-9][0-9]|20[0-2][0-9])$"

    if re.match(expresion_regular, entry_fechaNaci.get()):
        return True
    else:
        return False
  except ValueError:
    return False
  
def validar_correo():
  try:
    expresion_regular = r"(^[a-zA-Z0-9_.+-]+@(?!elpoli\.edu\.co)[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)|(^([a-z]+_[a-z]+\d{2}\d{2}[1-2]@elpoli\.edu\.co)$)"

    if re.match(expresion_regular, entry_mail.get()):
        return True
    else:
        return False
  except ValueError:
    return False

def validar_clave():
  try:
    expresion_regular = r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[_$#@])\S{8,}$"

    if re.match(expresion_regular, entry_pass.get()):
        return True
    else:
        return False
  except ValueError:
    return False
  
def validar_nuip():
  try:
    expresion_regular = r"^[0-9]{10}$"

    if re.match(expresion_regular, entry_nuip.get()) and entry_tdoc.get() == "Cedula de Ciudadania":
        return True
    elif entry_tdoc.get() != "Cedula de Ciudadania" and entry_tdoc.get() != "" and entry_nuip.get() != "":
        return True
    else:
        return False
  except ValueError:
    return False
  
def validar_y_procesar():
  if validar_fecha() and validar_clave() and validar_correo() and validar_nuip():
    generar_archivo(leer_archivo())
  elif not validar_nuip():
    messagebox.showerror("Error", "El NUIP no es válido.")
  elif not validar_clave():
    messagebox.showerror("Error", "La Clave no es válida.")
  elif not validar_fecha():
    messagebox.showerror("Error", "La Fecha no es válida.")
  elif not validar_correo():
    messagebox.showerror("Error", "El Correo no es válido.")
  
def tablacargar():
  try:
    tabla.delete(*tabla.get_children())
    for fila in leer_archivo():
      tabla.insert("", "end", values=fila)
  except:
    messagebox.showwarning("Error", "No se pudo leer la tabla")

def recuperar_clave():
  global entrys_nuip, entrys_mail
  recover = tk.Tk()
  recover.title("Recuperar Clave")
  frame_recuperar = tk.Frame(recover)
  frame_recuperar.pack()
  label_recuperar = tk.Label(frame_recuperar, text="Recuperar Clave")
  label_recuperar.pack()
  label_nuip = tk.Label(frame_recuperar, text="NUIP")
  label_nuip.pack()
  entrys_nuip = tk.Entry(frame_recuperar)
  entrys_nuip.pack()
  label_mail = tk.Label(frame_recuperar, text="Correo")
  label_mail.pack()
  entrys_mail = tk.Entry(frame_recuperar)
  entrys_mail.pack()
  button_recuperar = tk.Button(frame_recuperar, text="Recuperar Clave" , command=recuperacion)
  button_recuperar.pack()
  nb.add(frame_recuperar, text = "Recuperar Clave")
  recover.mainloop()
def recuperacion():
  try:
    for i in leer_archivo():
      if i[1] == entrys_nuip.get() and i[6] == entrys_mail.get():
        messagebox.showinfo("Informacion", "La clave es: " + desencriptar(i[3], token(i[4])))
  except:
     messagebox.showwarning("Error", "No hubo resultados")

def validar():
  try:
    if entrylog_mail.get() == "" or entrylog_pass.get() == "":
      messagebox.showerror("Error", "Por favor, rellene todos los campos.")
    else:
      for i in leer_archivo():
        if entrylog_mail.get() == i[6] and entrylog_pass.get() == desencriptar(i[3], token(i[4])) :
          messagebox.showinfo("Informacion", "Bienvenido")
          ingresar(i[2], i[1], i[4])
  except:
    messagebox.showwarning("Error", "No hay resultados")

def validar_cambiar_clave(clave):
  try:
    expresion_regular = r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[_$#@])\S{8,}$"

    if re.match(expresion_regular, clave):
        return True
    else:
        return False
  except ValueError:
    return False

def cambiar_clave():
    nueva_clave = entry_chpass.get()
    if validar_cambiar_clave(nueva_clave):
        clave_encriptada = encriptar(nueva_clave, token(superfecha))
        contenido = leer_archivo()
        for i in contenido:
            if i[1] == supernuip:
                i[3] = clave_encriptada
        generar_archivo(contenido)
        messagebox.showinfo("Información", "La clave ha sido cambiada exitosamente.")
    else:
        messagebox.showerror("Error", "La nueva clave no es válida.")
def ingresar(name, nuip, fecha):
  global entry_chpass, supernuip, superfecha
  supernuip = nuip
  superfecha = fecha
  join = tk.Tk()
  join.title("Bienvenido")
  frame_ingreso = tk.Frame(join)
  frame_ingreso.pack()
  label_ingreso = tk.Label(frame_ingreso, text="Bienvenido " + name)
  label_ingreso.pack()
  label_pass = tk.Label(frame_ingreso, text="Clave")
  label_pass.pack()
  entry_chpass = tk.Entry(frame_ingreso)
  entry_chpass.pack()
  button_change = tk.Button(frame_ingreso, text="Cambiar Clave", command=cambiar_clave)
  button_change.pack()

root = tk.Tk()
root.title("Interfaz de usuario")

nb = ttk.Notebook(root)
nb.pack(fill = "both", expand = "yes")

frame_usuarios = tk.Frame(root)
frame_usuarios.pack()
label_usuarios = tk.Label(frame_usuarios, text="Usuarios")
label_usuarios.pack()
tabla = ttk.Treeview(frame_usuarios)
tabla.config(columns=("Tipo Documento", "Nuip", "Nombres", "Token", "Fecha de Nacimiento", "Ciudad", "Correo", "Universidad"))
tabla.heading("Tipo Documento", text="Tipo Documento")
tabla.heading("Nuip", text="Nuip")
tabla.heading("Nombres", text="Nombres")
tabla.heading("Token", text="Token")
tabla.heading("Fecha de Nacimiento", text="Fecha de Nacimiento")
tabla.heading("Ciudad", text="Ciudad")
tabla.heading("Correo", text="Correo")
tabla.heading("Universidad", text="Universidad")
tabla.pack()
button_recargar = tk.Button(frame_usuarios, text="Actualizar tabla", command=tablacargar)
button_recargar.pack()
scrollbar = ttk.Scrollbar(frame_usuarios, orient="vertical", command=tabla.yview)
scrollbar.pack(side="right", fill="y")
tabla.configure(yscrollcommand=scrollbar.set)
nb.add(frame_usuarios, text = "Usuarios")
tablacargar()


frame_ingreso = tk.Frame(root)
frame_ingreso.pack()
label_ingreso = tk.Label(frame_ingreso, text="Ingreso de usuario")
label_ingreso.pack()
label_mail = tk.Label(frame_ingreso, text="Correo")
label_mail.pack()
entrylog_mail = tk.Entry(frame_ingreso)
entrylog_mail.pack()
label_pass = tk.Label(frame_ingreso, text="Contraseña")
label_pass.pack()
entrylog_pass = tk.Entry(frame_ingreso)
entrylog_pass.pack()
button_ingresar = tk.Button(frame_ingreso, text="Ingresar", command=validar)
button_ingresar.pack()
button_forgette = tk.Button(frame_ingreso, text="Olvide Contraseña" , command=recuperar_clave)
button_forgette.pack()
nb.add(frame_ingreso, text = "Ingreso")


  
frame_registro = tk.Frame(root)
frame_registro.pack()
label_registro = tk.Label(frame_registro, text="Registro de usuario")
label_registro.pack()
label_tdoc = tk.Label(frame_registro, text="Tipo de documento")
label_tdoc.pack()
entry_tdoc = ttk.Combobox(frame_registro, values=["Cedula de Ciudadania", "Tarjeta de Identidad", "Pasaporte"])
entry_tdoc.pack()
label_nuip = tk.Label(frame_registro, text="Numero de Identidad")
label_nuip.pack()
entry_nuip = tk.Entry(frame_registro)
entry_nuip.pack()
label_name = tk.Label(frame_registro, text="Nombre")
label_name.pack()
entry_name = tk.Entry(frame_registro)
entry_name.pack()
label_pass = tk.Label(frame_registro, text="Contraseña")
label_pass.pack()
entry_pass = tk.Entry(frame_registro)
entry_pass.pack()
label_fechaNaci = tk.Label(frame_registro, text="Fecha de Nacimiento (DD/MM/AAAA)")
label_fechaNaci.pack()
entry_fechaNaci = tk.Entry(frame_registro)
entry_fechaNaci.pack()
label_city = tk.Label(frame_registro, text="Ciudad")
label_city.pack()
entry_city = tk.Entry(frame_registro)
entry_city.pack()
label_mail = tk.Label(frame_registro, text="Correo")
label_mail.pack()
entry_mail = tk.Entry(frame_registro)
entry_mail.pack()
label_university = tk.Label(frame_registro, text="Universidad")
label_university.pack()
entry_university = tk.Entry(frame_registro)
entry_university.pack()
button_ingresar = tk.Button(frame_registro, text="Registrar", command=validar_y_procesar)
button_ingresar.pack()
nb.add(frame_registro, text = "Registro")

root.mainloop()