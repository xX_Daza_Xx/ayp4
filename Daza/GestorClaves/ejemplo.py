def encriptar(mensaje, token):
    # Esta función encripta el mensaje utilizando el token
    cifrado = ""
    for caracter in mensaje:
        if caracter.isalpha():
            ascii_offset = 65 if caracter.isupper() else 97
            cifrado += chr((ord(caracter) - ascii_offset + token) % 26 + ascii_offset)
        else:
            cifrado += caracter
    return cifrado

def desencriptar(mensaje, token):
    # Esta función desencripta el mensaje utilizando el token
    descifrado = ""
    for caracter in mensaje:
        if caracter.isalpha():
            ascii_offset = 65 if caracter.isupper() else 97
            descifrado += chr((ord(caracter) - ascii_offset - token) % 26 + ascii_offset)
        else:
            descifrado += caracter
    return descifrado

def token(fecha):
  fecha_sin_barras = fecha.replace("/", "")
  numeros = list(map(int, fecha_sin_barras))
  suma = sum(numeros)
  return suma

token = token("23/03/1987")
mensaje = "SuperClave"
mensaje_encriptado = encriptar(mensaje, token)
mensaje_desencriptado = desencriptar(mensaje_encriptado, token)

print(f"Mensaje original: {mensaje}")
print(f"Mensaje encriptado: {mensaje_encriptado}")
print(f"Mensaje desencriptado: {mensaje_desencriptado}")

