import tkinter as tk
from tkinter import messagebox
import numpy as np
import time

def encontrar_indices(matriz, numero):
    indices = []
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            if matriz[i][j] == numero:
                indices.append([i, " " ,j])
    return indices
    
def buscar():
    matriz = np.loadtxt('Daza/Matriz/numeros.txt', dtype=int, delimiter=',')
    ocurrencias = encontrar_indices(matriz, int(entry.get()))
    tiempo_total = time.perf_counter()

    if ocurrencias:
        tiempo_inicio = time.perf_counter()
        _ = ocurrencias[0]
        tiempo_primer_numero = time.perf_counter() - tiempo_inicio
        
        tiempo_inicio = time.perf_counter()
        _ = ocurrencias[len(ocurrencias) // 2]
        tiempo_numero_medio = time.perf_counter() - tiempo_inicio
        
        tiempo_inicio = time.perf_counter()
        _ = ocurrencias[-1]
        tiempo_ultimo_numero = time.perf_counter() - tiempo_inicio

        messagebox.showinfo("Encontrado", f"Tiempo para encontrar el primer número: {tiempo_primer_numero * 1e6:.2f} µs\nTiempo para encontrar el número del medio: {tiempo_numero_medio * 1e6:.2f} µs\nTiempo para encontrar el último numero: {tiempo_ultimo_numero * 1e6:.2f} µs")
        
    else:
        print("El número no se encontró en la matriz.")

    messagebox.showinfo("Tiempo total de búsqueda", f"Tiempo total de búsqueda: {tiempo_total * 1e6:.2f} µs")

    root.destroy()

root = tk.Tk()
root.title("Buscador")

label = tk.Label(root, text="Ingrese el número a buscar:")
label.pack()
entry = tk.Entry(root)
entry.pack()

button = tk.Button(root, text="Buscar", command=buscar)
button.pack()

root.mainloop()