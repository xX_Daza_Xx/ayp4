from tkinter import messagebox
import numpy as np

Archivo = "Daza/Matriz/numeros.txt"
var = 1987

def generar_archivo(contenido):
  contenido[0][0] = var
  contenido[499][499] = var
  contenido[999][999] = var
  
  try:
    with open(Archivo, "w") as archivo:
      for fila in contenido:
        archivo.write(','.join(map(str, fila)) + '\n')
    archivo.close()
    print(f"El archivo {Archivo} se ha creado exitosamente.")
  except Exception as error:
    messagebox.showerror("ERROR", "Error al crear el archivo: " + str(error))

generar_archivo(np.random.randint(0, 501, size=(1000, 1000)))

